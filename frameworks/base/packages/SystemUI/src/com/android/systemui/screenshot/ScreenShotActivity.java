package com.android.systemui.screenshot;
 
import android.app.Activity;
import android.os.Bundle;
import com.android.systemui.R;
 
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.os.Message;
import android.os.Messenger;
import android.os.Handler;
import android.os.IBinder;
import android.os.UserHandle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.content.Context;
import android.content.Intent;
 
public class ScreenShotActivity extends Activity {
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quick_settings_screenshot);
 
        mHandler.postDelayed(mScreenshotRunnable, 100);
    }
 
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (mScreenshotConnection != null) {
                ScreenShotActivity.this.unbindService(mScreenshotConnection);
                mScreenshotConnection = null;
            }
            ScreenShotActivity.this.finish();
        }
    };
 
    final Object mScreenshotLock = new Object();
    ServiceConnection mScreenshotConnection = null;
 
    final Runnable mScreenshotTimeout = new Runnable() {
        @Override public void run() {
            synchronized (mScreenshotLock) {
                mHandler.sendEmptyMessage(0);
            }
        }
    };
 
    private final Runnable mScreenshotRunnable = new Runnable() {
        @Override
        public void run() {
            takeScreenshot();
        }
    };
 
    private void takeScreenshot() {
        synchronized (mScreenshotLock) {
            if (mScreenshotConnection != null) {
                mHandler.sendEmptyMessage(0);
                return;
            }
            ComponentName cn = new ComponentName("com.android.systemui",
                    "com.android.systemui.screenshot.TakeScreenshotService");
            Intent intent = new Intent();
            intent.setComponent(cn);
            ServiceConnection conn = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {
                    synchronized (mScreenshotLock) {
                        if (mScreenshotConnection != this) {
                            mHandler.sendEmptyMessage(0);
                            return;
                        }
                        Messenger messenger = new Messenger(service);
                        Message msg = Message.obtain(null, 1);
                        final ServiceConnection myConn = this;
                        Handler h = new Handler(mHandler.getLooper()) {
                            @Override
                            public void handleMessage(Message msg) {
                                synchronized (mScreenshotLock) {
                                    if (mScreenshotConnection == myConn) {
                                        ScreenShotActivity.this.unbindService(mScreenshotConnection);
                                        mScreenshotConnection = null;
                                        mHandler.removeCallbacks(mScreenshotTimeout);
                                    }
                                }
                            }
                        };
                        msg.replyTo = new Messenger(h);
                        msg.arg1 = msg.arg2 = 0;
                        try {
                            messenger.send(msg);
                        } catch (RemoteException e) {
                        }
                        mHandler.sendEmptyMessage(0);
                    }
                }
                @Override
                public void onServiceDisconnected(ComponentName name) {}
            };
            if (ScreenShotActivity.this.bindServiceAsUser(
                    intent, conn, Context.BIND_AUTO_CREATE, UserHandle.CURRENT)) {
                mScreenshotConnection = conn;
                mHandler.postDelayed(mScreenshotTimeout, 10000);
            }
        }
    }
 
}
